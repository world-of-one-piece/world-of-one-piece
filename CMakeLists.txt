cmake_minimum_required(VERSION 2.6)

project(Woop)

add_definitions(-std=c++0x)

include_directories(
    ./OA2e/include
    /usr/include/sigc++-2.0
    /usr/lib/sigc++-2.0/include
)
set(CMAKE_MODULE_PATH "./cmake/")
#set(CMAKE_CXX_FLAGS "-Wall -W -Werror -ansi -pedantic -g")
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/lib)


find_package(
  SFML
  REQUIRED audio graphics window system 
)

include_directories (${SFML_INCLUDE_DIR})

set ( 
  COMMONS_DEPENDS
  ${SFML_AUDIO_LIBRARY}
  ${SFML_GRAPHICS_LIBRARY}
  ${SFML_WINDOW_LIBRARY}
  ${SFML_SYSTEM_LIBRARY}
  sigc-2.0
)

add_subdirectory ( client )
add_subdirectory ( editeur )
add_subdirectory ( OA2e )
