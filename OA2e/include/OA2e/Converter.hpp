

#ifndef OA2E_CONVERTER_HPP
#define OA2E_CONVERTER_HPP

#include <string>
#include <sstream>


namespace OA2e
{

template <typename T>
std::string str(const T& i);

template <typename T>
T conv(const std::string& str);

} // namespace OA2e

#include <OA2e/Converter.inl>

#endif // OA2E_CONVERTER_HPP
	

