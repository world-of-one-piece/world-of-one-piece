
namespace OA2e
{


template <typename T>
std::string str(const T& i) {
    std::ostringstream stream;
    stream << i;
    return stream.str();
}

template <typename T>
T conv(const std::string& str) {
    std::istringstream stream(str);
    T tmp;
    stream >> tmp;
    return tmp;
}

}
