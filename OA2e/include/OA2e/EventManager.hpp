///
/// \file EventManager.hpp
/// \author Alexandre.Janniaux
/// \brief Handle and send received events
///
#ifndef EVENT_MANAGER_HPP
#define EVENT_MANAGER_HPP

#include <string>
#include <map>
#include <OA2e/Handler.hpp>
#include <SFML/Window/Event.hpp>
#include <sigc++/sigc++.h>
#include <SFML/System/Vector2.hpp>

namespace OA2e {

///
/// \class EventManager
/// \brief Handle and send received events
///
/// Handle and send received events
/// EventManager are autoshared and autodeleted
///
class EventManager {

public:
    ///
    /// \brief Push event 
    /// \param event : an event to push
    ///
    void Apply 		(const sf::Event& event);
    
    ///
    ///
    ///
    void Update		(const float& timeElapsed);
  

    sigc::signal<void> 			OnExit;
    sigc::signal<void, sf::Vector2i>	OnResize;
    sigc::signal<void, sf::Event::KeyEvent> 	
					OnKeyPressed;
    sigc::signal<void, sf::Event::KeyEvent> 	
					OnKeyReleased;
    sigc::signal<void, sf::Uint32>	OnTextEntered;
    sigc::signal<void, sf::Vector2i > 	OnMouseMoved;
    sigc::signal<void, sf::Mouse::Button ,sf::Vector2i >
					OnMouseButtonPressed;
    sigc::signal<void, sf::Mouse::Button ,sf::Vector2i >
					OnMouseButtonReleased;
    sigc::signal<void, sf::Event::JoyMoveEvent>
					OnJoyMoved;
    sigc::signal<void, sf::Event::JoyButtonEvent >	
					OnJoyButtonPressed;
    sigc::signal<void, sf::Event::JoyButtonEvent >	
					OnJoyButtonReleased;
    sigc::signal<void, float>		OnUpdate;
    
					
private:
    EventManager();
    
    friend class Handler<EventManager>;
  
};

typedef Handler<EventManager> EventManagerHandler;

  
  
} // namespace OA2e

#endif // EVENT_MANAGER_HPP