///
///
///
///

#ifndef HANDLER_HPP
#define HANDLER_HPP

namespace OA2e {

///
///
///
template <typename T>
class Handler {
  
   struct Handle {
      unsigned short int Refs;
      T* Handle;
   };
  
  
   typedef  std::map < std::string, Handler<T>::Handle > 	HandlesMap;
   
   
   
  
public:
    
    ///
    /// \brief Constructor
    /// \param name : manager's share name
    ///
    /// Increment share counter
    ///
    Handler		(const std::string& name="");
  
    ///
    /// \brief Destructor
    ///
    /// Decrement share counter
    ///
    ~Handler	();
    
    ///
    /// \brief Wrap handle
    ///
    T& operator* ();
    
    ///
    ///
    ///
    T* operator-> ();
  
private:
     T*			m_Handle;
     std::string	m_HandleName;
     
     ///
     /// Handle managers
     ///
     static HandlesMap  	m_Handles;
  
};


} // namespace OA2e

#include <OA2e/Handler.inl>

#endif // HANDLER_HPP