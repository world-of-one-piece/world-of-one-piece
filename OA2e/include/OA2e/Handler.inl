///
///
///
///

namespace OA2e {

///
///
///
//*
template <typename T>
typename Handler<T>::HandlesMap
Handler<T>::m_Handles;
//*/


template <typename T>
Handler<T>::Handler (const std::string& name) {
    if(m_Handles.find(name) == m_Handles.end()) {
	Handle handle;
	handle.Handle = new T;
	m_Handles[name] = handle;
    }
    m_Handle = m_Handles.at(name).Handle;
    m_HandleName = name;
    m_Handles.at(name).Refs++;
}

template <typename T>
Handler<T>::~Handler () {
    if(--m_Handles.at ( m_HandleName ).Refs == 0) {
	delete m_Handles.at ( m_HandleName ).Handle;
	m_Handles.erase ( m_Handles.find( m_HandleName ) );
    }
	
}

template <typename T>
T&
Handler<T>::operator*() {
    return *m_Handle;
}

template <typename T>
T*
Handler<T>::operator->() {
    return m_Handle;
}

} // namespace OA2e
