#ifndef OA2E_IMAGE_ASSET_HPP
#define OA2E_IMAGE_ASSET_HPP

#include <OA2e/ResourceManager.hpp>
#include <OA2e/Handler.hpp>
#include <SFML/Graphics/Image.hpp>

namespace OA2e {

class ImageAsset {

public:
    ImageAsset ( const std::string& filename, const std::string& context="" );
    
    operator const sf::Image& ();
  
private:
    typedef sf::Image Handle;
  
    friend class ResourceManager<ImageAsset>;
    
    static sf::Image*
    Load ( const std::string& filename, const std::string& context);
    
    static void
    Unload (sf::Image * image, std::string context);
    
    sf::ResourcePtr<sf::Image> 			m_Image;
    Handler< ResourceManager<ImageAsset> >	m_Handle;
  
}; // class ImageAsset

typedef ResourceManager<ImageAsset> ImageManager;
typedef Handler<ImageManager> ImageHandler;

} // namespace OA2e

#endif // OA2E_IMAGE_ASSET_HPP