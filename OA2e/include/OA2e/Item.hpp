#ifndef OA2E_ITEM_HPP
#define OA2E_ITEM_HPP
#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

namespace OA2e {

class Item {
  
public:
    
    //void AttachAnimation ( );
    //void AttachZone ( );
    //void AttachCharset ( );
    void SetImage ( const sf::Image& image );
    
    void Render ( sf::RenderTarget& target ) const;
  
private:
    sf::Sprite m_Sprite;
    
};

}

#endif // OA2E_ITEM_HPP