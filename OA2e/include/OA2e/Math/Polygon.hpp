#ifndef OA2E_MATH_POLYGON_HPP
#define OA2E_MATH_POLYGON_HPP
#include <SFML/System/Vector2.hpp>
#include <list>

namespace OA2e {
 
class Polygon {
  
public:
    Polygon 		( ) ;
    Polygon 		( const std::initializer_list<sf::Vector2f>& points) ;
  
    void 
    AddPoint		( const sf::Vector2f& point ) ;
    
    void
    RemovePoint 	( const unsigned int& index ) ;
    
    Polygon&
    operator << 	( const sf::Vector2f& point ) ;
    
    Polygon&
    operator << 	( const std::initializer_list<sf::Vector2f>& points ) ;
    
    const std::list<sf::Vector2f>&
    GetPoints		( ) const ;
    
    bool
    IsConvex		( ) const ;
    
    std::list<Polygon>
    MakeConvex		( ) const ;
    
    bool
    Intersect		( const Polygon& other ) const ;
    
    Polygon
    GetIntersection 	( const Polygon& other ) const ;
    
    float
    GetPerimeter	() const ;
    
    float
    GetArea		() const ;
    
private:
    std::list<sf::Vector2f>	m_Points;
    float			m_Perimeter;
    float			m_Area;
    bool			m_IsConvex;
    
    bool			m_NeedComputeConvex;
    bool			m_NeedComputePerimeter;
    bool			m_NeedComputeArea;
};
  
  
  
}


#endif // OA2E_MATH_POLYGON_HPP