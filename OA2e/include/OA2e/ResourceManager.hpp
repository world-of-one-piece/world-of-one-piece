#ifndef OA2E_RESOURCE_MANAGER_HPP
#define OA2E_RESOURCE_MANAGER_HPP

#include <string>
#include <map>
#include <algorithm>

namespace OA2e {
  
template <class T>
class ResourceManager {
  
    ///
    ///
    ///
    ///
    struct Handle {
	std::string Filename;
	std::string Context;
	typename T::Handle* Resource;
    };
    
    typedef std::map < std::string, ResourceManager<T>::Handle > 	ResourcesMap;
  
public:
  
    ///
    /// \brief Destructor
    ///
    /// Free all resources
    ///
    ~ResourceManager();
  
    ///
    /// \brief load and get any resource
    /// \param filename : resource's filename 
    /// \param context	: context to load subresources
    /// \return Resource
    ///
    const typename T::Handle& 
    Get		(const std::string& filename, 
		const std::string& context="");
  
  
    ///
    /// \brief remove any resource by filename
    /// \param filename : resource's filename 
    /// \param context	: context to unload subresources
    ///
    void
    Remove	(const std::string& filename);
    
    ///
    /// \brief remove any resource by reference
    /// \param resource : resource to remove
    ///
    void
    Remove	(const typename T::Handle& resource);
    
    ///
    /// \brief check if resource is already loaded
    /// \param filname : resource's filename
    /// \return true if loaded, false otherwise
    ///
    bool
    Has		(const std::string& filename);
    
    ///
    /// \brief check if resource is in manager
    /// \param resource : resource to check
    /// \return true if in manager, false otherwise
    ///
    bool
    Has		(const typename T::Handle& resource);
    
    ///
    /// \brief get info about resource
    /// \param resource : the resource 
    /// \return handle to resource
    ///
    Handle
    FindResource (const typename T::Handle& resource);
    
private:
  
    ResourcesMap 	m_Resources;
        
}; // class ResourceManager
  
  
} // namespace OA2e

#include <OA2e/ResourceManager.inl>


#endif // OA2E_RESOURCE_MANAGER_HPP