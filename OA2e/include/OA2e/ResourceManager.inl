
namespace OA2e {

template <class T>
ResourceManager<T>::~ResourceManager() {

    typename ResourcesMap::iterator it = m_Resources.begin();
    while ( it != m_Resources.end() ) {
	T::Unload ((it->second).Resource, (it->second).Context);
	m_Resources.erase(it);
    }

}

template <class T>
const typename T::Handle&
ResourceManager<T>::Get		(const std::string& filename,
				const std::string& context) {

    if (!Has(filename)) {
	Handle handle;
	handle.Filename = filename;
	handle.Context = context;
	handle.Resource = T::Load (filename, context);
	m_Resources[filename] =  handle;
    }

    return *m_Resources[filename].Resource;
}

template <class T>
void
ResourceManager<T>::Remove	(const std::string& filename) {
    if (Has(filename)) {
	T::Unload	(m_Resources[filename].Resource, m_Resources[filename].Context);
	m_Resources.remove(filename);
    }
  
}

template <class T>
void
ResourceManager<T>::Remove	(const typename T::Handle& resource) {
    if (Has(resource)) {
	Handle handle = FindResource (resource);
	if (handle.Resource != &resource)
	    return; // TODO: launch exception
	T::Unload (&resource, handle.Context);
	m_Resources.erase ( std::find( m_Resources.begin(), m_Resources.end(), handle ) );
    }
}


template <class T>
bool
ResourceManager<T>::Has		(const std::string& filename) {
    return m_Resources.find (filename) != m_Resources.end();
}

template <class T>
bool
ResourceManager<T>::Has		(const typename T::Handle& resource) {
    return FindResource (&resource).Resource != NULL;
}

template <class T>
typename ResourceManager<T>::Handle
ResourceManager<T>::FindResource (const typename T::Handle& resource) {
    auto it = m_Resources.begin();
    while (it != m_Resources.end()) {
	if (it->second.Resource == &resource)
	  return it->second;
    }
    return Handle();
}

} // namespace OA2e