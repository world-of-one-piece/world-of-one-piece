#ifndef OA2E_SOUND_ASSET_HPP
#define OA2E_SOUND_ASSET_HPP

#include <OA2e/ResourceManager.hpp>
#include <OA2e/Handler.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

namespace OA2e {

class SoundAsset {

public:
    SoundAsset ( const std::string& filename, const std::string& context="" );
    
    operator const sf::SoundBuffer& ();
  
private:
    typedef sf::SoundBuffer Handle;
  
    friend class ResourceManager<SoundAsset>;
    
    static sf::SoundBuffer*
    Load ( const std::string& filename, const std::string& context);
    
    static void
    Unload (sf::SoundBuffer* sound, std::string context);
    
    sf::ResourcePtr<sf::SoundBuffer> 		m_Sound;
    Handler< ResourceManager<SoundAsset> > m_Handle;
  
}; // class ImageAsset

typedef ResourceManager<SoundAsset> 	SoundManager;
typedef Handler<SoundManager> 		SoundHandler;

} // namespace OA2e

#endif // OA2E_SOUND_ASSET_HPP