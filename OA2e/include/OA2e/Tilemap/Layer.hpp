#ifndef OA2E_TILEMAP_LAYER_HPP
#define OA2E_TILEMAP_LAYER_HPP
#include <map>
#include <SFML/Graphics/RenderTarget.hpp>

namespace OA2e {

class Tile;
class Tilemap;  
  
///
/// \brief Hold tiles as a layer
///
class Layer {
  
public:
  
  ///
  /// \brief Constructor
  /// \param sizex : layer's width
  /// \param sizey : layer's height
  ///
  Layer ( const unsigned int& sizex, const unsigned int& sizey);
  
  ///
  /// \todo doc
  ///
  Tile&
  At 	( const unsigned int& x, const unsigned int& y);
  
protected:
  ///
  /// 
  ///
  virtual void 
  Render ( sf::RenderTarget& target ) const;
  
private:
  friend void Tilemap::Render ( sf::RenderTarget& target ) const;
  
  std::map <unsigned int, std::map <unsigned int, Tile> > m_Tiles;
  
}; // class Layer

} // namespace OA2e

#endif OA2E_TILEMAP_LAYER_HPP