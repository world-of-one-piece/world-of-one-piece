#ifndef OA2E_TILEMAP_TILE_HPP
#define OA2E_TILEMAP_TILE_HPP
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>


namespace OA2e {

class Layer;
  
class Tile {
  
public:
  
protected:
  
  virtual void 
  Render ( sf::RenderTarget& target ) const;
  
private:
  friend void Render ( sf::RenderTarget& target ) const;
  
  sf::Sprite 	m_Sprite;
  
}; // class Tile

} // namespace OA2e

#endif // OA2E_TILEMAP_TILE_HPP