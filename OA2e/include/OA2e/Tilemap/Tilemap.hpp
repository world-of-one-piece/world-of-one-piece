#ifndef OA2E_TILEMAP_HPP
#define OA2E_TILEMAP_HPP

#include <SFML/System/Vector2.hpp>
#include <map>

namespace OA2e {

class Layer;
class Tile;

///
/// \brief hold a tilemap
///
class Tilemap {

public:
  ///
  /// \brief Constructor
  /// \param sizex : tilemap's width
  /// \param sizey : tilemap's height
  /// construct a tilemap with width and height
  ///
  Tilemap	(const unsigned int& sizex, const unsigned int& sizey);
  
  ///
  /// \brief Define tile size and resize sprites
  /// \param tilesize : new tile size
  ///
  void
  SetTileSize	(const sf::Vector2f& tilesize);
  
  ///
  /// \brief Access layer in a tilemap
  /// \param layer : layer's position
  ///
  /// Access layer in a tilemap, if layer is out of range, it will be created
  ///
  Layer&
  operator [] (const unsigned int& layer);
  
protected:
  
  virtual void 
  Render ( sf::RenderTarget& target ) const;
  
private:
  
  std::map <unsigned int,Layer>	
			m_Layer;
  sf::Vector2i		m_Size;
  sf::Vector2f 		m_Tilesize;
}; // class Tilemap

} // namespace OA2e

#endif


