#include <OA2e/EventManager.hpp>

namespace OA2e {
  
EventManager::EventManager() { 
}

void
EventManager::Apply ( const sf::Event& event) {
  
    ///////////////////
    // window events //
    ///////////////////
    if (event.Type == sf::Event::Closed)
	OnExit();
    
    else if (event.Type == sf::Event::Resized)
	OnResize(sf::Vector2i(event.Size.Height, event.Size.Width));
    
    
    //////////////////
    // mouse events //
    //////////////////
    else if (event.Type == sf::Event::MouseMoved) 
	OnMouseMoved(sf::Vector2i(event.MouseMove.X, event.MouseMove.Y));
    
    else if (event.Type == sf::Event::MouseButtonPressed)
	OnMouseButtonPressed(event.MouseButton.Button, sf::Vector2i(event.MouseButton.X, event.MouseButton.Y));
    
    else if (event.Type == sf::Event::MouseButtonReleased)
	OnMouseButtonReleased(event.MouseButton.Button, sf::Vector2i(event.MouseButton.X, event.MouseButton.Y));
    
    ////////////////
    // key events //
    ////////////////
    else if (event.Type == sf::Event::KeyPressed)
	OnKeyPressed(event.Key);
    
    else if (event.Type == sf::Event::KeyReleased)
	OnKeyReleased(event.Key);
    
    else if (event.Type == sf::Event::TextEntered)
	OnTextEntered(event.Text.Unicode);
    
    ////////////////
    // joy events //
    ////////////////
    else if (event.Type == sf::Event::JoyMoved)
	OnJoyMoved(event.JoyMove);
      

    else if (event.Type == sf::Event::JoyButtonPressed)
	OnJoyButtonPressed(event.JoyButton);
      
    else if (event.Type == sf::Event::JoyButtonReleased)
	OnJoyButtonReleased(event.JoyButton);
    
}

void
EventManager::Update (const float& elapsedTime) {
    OnUpdate(elapsedTime);
}
  
}