#include <OA2e/ImageAsset.hpp>

namespace OA2e {

ImageAsset::ImageAsset ( const std::string& filename, 
			 const std::string& context) :
	m_Handle(context)
			 {
	m_Image = &m_Handle->Get(filename);
}

ImageAsset::operator const sf::Image& () {
    return (*m_Image);
}

sf::Image*
ImageAsset::Load ( const std::string& filename, 
		   const std::string& context) {
  
    sf::Image* image = new sf::Image;
    image->LoadFromFile(filename);
    return image;
}

void
ImageAsset::Unload (sf::Image* image, 
		    std::string context) {
    delete image;
}

}