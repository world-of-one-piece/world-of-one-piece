
#include <OA2e/Item.hpp>

namespace OA2e {
  
void
Item::SetImage ( const sf::Image& image ) {
    m_Sprite.SetImage(image);
}
  
void
Item::Render ( sf::RenderTarget& target ) const {
    target.Draw(m_Sprite);
}

}


