#include <OA2e/SoundAsset.hpp>

namespace OA2e {
  
SoundAsset::SoundAsset ( const std::string& filename, 
			 const std::string& context) : 
    m_Handle(context)
			 {
	m_Sound = &m_Handle->Get(filename);
}

SoundAsset::operator const sf::SoundBuffer& () {
    return (*m_Sound);
}

sf::SoundBuffer*
SoundAsset::Load ( const std::string& filename, 
		   const std::string& context) {
  
    sf::SoundBuffer* sound = new sf::SoundBuffer;
    sound->LoadFromFile(filename);
    return sound;
}

void
SoundAsset::Unload (sf::SoundBuffer* sound, 
		    std::string context) {
    delete sound;
}

  
}