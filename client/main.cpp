#include <SFML/Graphics.hpp>
#include <OA2e/EventManager.hpp>
#include <OA2e/Converter.hpp>
#include <OA2e/ResourceManager.hpp>
#include <OA2e/ImageAsset.hpp>
#include <OA2e/SoundAsset.hpp>
#include <OA2e/Item.hpp>
#include <SFML/Audio.hpp>


sf::Text fps;

void UpdateFPS (float elapsedTime) {
  fps.SetString("FPS : " + OA2e::str((int)(1/elapsedTime)));
}

int main (int argc, char** argv) {

  fps.SetString("FPS :");
  
  sf::VideoMode videomode ( 800, 600, 32 );
  sf::RenderWindow window ( videomode,
			    "World of One Piece" );
  
  window.SetFramerateLimit(60);
  
  sf::Event event;
  
  OA2e::EventManagerHandler eventManager;
      eventManager->OnExit.connect(sigc::mem_fun(window, &sf::RenderWindow::Close));
      eventManager->OnKeyPressed.connect(sigc::hide(sigc::mem_fun(window, &sf::RenderWindow::Close)));
      eventManager->OnUpdate.connect(&UpdateFPS);
      
  //OA2e::ImageHandler imageManager;
  OA2e::SoundHandler soundManager;
      
  sf::Sound sound;
    sound.SetBuffer (OA2e::SoundAsset("data/sound.wav"));
    //sound.Play();
  
  sf::Sound music;
    music.SetBuffer (OA2e::SoundAsset("data/music.ogg"));
    music.Play();
  
  //sf::Sprite test (OA2e::ImageAsset("data/test.png"));
  //test.Move(200.f,200.f);
  
  OA2e::Item item;
    item.SetImage (OA2e::ImageAsset("data/test.png"));
  
  while ( window.IsOpened() ) {
      while ( window.GetEvent ( event ) )
	  eventManager->Apply(event);
      
      window.Clear();
      
      window.Draw(fps);
      item.Render(window);
      
      window.Display();
      
      eventManager->Update (window.GetFrameTime());

  }
  
}